<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    public $transformer = Transformers\CategoryTransformer::class;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Massively assigned
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'pivot',
    ];


    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
