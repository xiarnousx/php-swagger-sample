<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     *  @todo figure out a way to create array of objects. currently displays json instead of array.
     *
     *  @SWG\Definition(
     *      definition="Category",
     *      required={"title", "details"},
     *     @SWG\Property(property="identifier", type="integer"),
     *     @SWG\Property(property="title", type="string"),
     *     @SWG\Property(property="details", type="string"),
     *     @SWG\Property(property="creationDate", type="string"),
     *     @SWG\Property(property="lastChange", type="string"),
     *     @SWG\Property(property="deletedDate", type="string"),
     *     @SWG\Property(
     *          property="links",
     *          type="object",
     *          @SWG\Property( property="rel", type="string"),
     *          @SWG\Property( property="href", type="string"),
     *
     *     ),
     *  )
     *
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'identifier'    => (int) $category->id,
            'title'         => (string) $category->name,
            'details'       => (string) $category->description,
            'creationDate'  => (string) $category->created_at,
            'lastChange'    => (string)$category->updated_at,
            'deletedDate'   => isset($category->deleted_at) ? (string) $category->deleted_at : null,

            'links' => [
                [
                    'rel'    => 'self',
                    'href'   => route('categories.show', $category->id),
                ],

                [
                    'rel'    => 'category.buyers',
                    'href'   => route('categories.buyers.index', $category->id),
                ],

                [
                    'rel'    => 'category.products',
                    'href'   => route('categories.products.index', $category->id),
                ],

                [
                    'rel'    => 'category.sellers',
                    'href'   => route('categories.sellers.index', $category->id),
                ],

                [
                    'rel'    => 'category.transactions',
                    'href'   => route('categories.transactions.index', $category->id),
                ],
            ],
        ];
    }


    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'title'         => 'name',
            'details'       => 'description',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'title'         => 'name',
            'details'       => 'description',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        $attributes = array_flip($attributes);

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
