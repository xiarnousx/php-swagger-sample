<?php

namespace App\Transformers;

use App\Buyer;
use League\Fractal\TransformerAbstract;

/**
 *  @SWG\Definition(
 *      definition="Buyer",
 *      required={"identifier", "name", "email"},
 *     @SWG\Property(property="identifier", type="string"),
 *     @SWG\Property(property="name", type="string"),
 *     @SWG\Property(property="email", type="string"),
 *     @SWG\Property(property="isVerified", type="boolean"),
 *     @SWG\Property(property="creationDate", type="string"),
 *     @SWG\Property(property="lastChange", type="string"),
 *     @SWG\Property(property="deletedDate", type="string"),
 *     @SWG\Property(
 *          property="links",
 *          type="object",
 *          @SWG\Property( property="rel", type="string"),
 *          @SWG\Property( property="href", type="string"),
 *
 *     ),
 *  )
 * Class BuyerTransformer
 * @package App\Transformers
 */
class BuyerTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Buyer $buyer)
    {
        return [
            'identifier'    => (int) $buyer->id,
            'name'          => (string) $buyer->name,
            'email'         => (string) $buyer->email,
            'isVerified'    => (int) $buyer->verified,
            'creationDate'  => (string)$buyer->created_at,
            'lastChange'    => (string)$buyer->updated_at,
            'deletedDate'   => isset($buyer->deleted_at) ? (string) $buyer->deleted_at : null,

            'links' => [
                [
                    'rel'   => 'self',
                    'href'  => route('buyers.show', $buyer->id),
                ],

                [
                    'rel'   => 'buyer.categories',
                    'href'  => route('buyers.categories.index', $buyer->id),
                ],

                [
                    'rel'   => 'buyer.products',
                    'href'  => route('buyers.products.index', $buyer->id),
                ],

                [
                    'rel'   => 'buyer.sellers',
                    'href'  => route('buyers.sellers.index', $buyer->id),
                ],

                [
                    'rel'   => 'buyer.transactions',
                    'href'  => route('buyers.transactions.index', $buyer->id),
                ],

                [
                    'rel'   => 'profile',
                    'href'  => route('users.show', $buyer->id),
                ],


            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'name'          => 'name',
            'email'         => 'email',
            'isVerified'    => 'verified',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }


    public static function transformedAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'name'          => 'name',
            'email'         => 'email',
            'isVerified'    => 'verified',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        $attributes = array_flip($attributes);

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
