<?php

namespace App\Transformers;

use App\Seller;
use League\Fractal\TransformerAbstract;

/**
 *  @todo figure out a way to create array of objects. currently displays json instead of array.
 *
 *  @SWG\Definition(
 *      definition="Seller",
 *      required={"identifier", "name", "email"},
 *     @SWG\Property(property="identifier", type="string"),
 *     @SWG\Property(property="name", type="string"),
 *     @SWG\Property(property="email", type="string"),
 *     @SWG\Property(property="isVerified", type="boolean"),
 *     @SWG\Property(property="creationDate", type="string"),
 *     @SWG\Property(property="lastChange", type="string"),
 *     @SWG\Property(property="deletedDate", type="string"),
 *     @SWG\Property(
 *          property="links",
 *          type="object",
 *          @SWG\Property( property="rel", type="string"),
 *          @SWG\Property( property="href", type="string"),
 *
 *     ),
 *  )
 * Class SellerTransformer
 * @package App\Transformers
 */
class SellerTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'identifier'    => (int) $seller->id,
            'name'          => (string) $seller->name,
            'email'         => (string) $seller->email,
            'isVerified'    => (int) $seller->verified,
            'creationDate'  => (string) $seller->created_at,
            'lastChange'    => (string) $seller->updated_at,
            'deletedDate'   => isset($seller->deleted_at) ? (string) $seller->deleted_at : null,


            'links' => [
                [
                    'rel'   => 'self',
                    'href'  => route('sellers.show', $seller->id),
                ],

                [
                    'rel'   => 'seller.buyers',
                    'href'  => route('sellers.buyers.index', $seller->id),
                ],

                [
                    'rel'   => 'seller.categories',
                    'href'  => route('sellers.categories.index', $seller->id),
                ],

                [
                    'rel'   => 'seller.products',
                    'href'  => route('sellers.products.index', $seller->id),
                ],

                [
                    'rel'   => 'seller.transactions',
                    'href'  => route('sellers.transactions.index', $seller->id),
                ],

                [
                    'rel'   => 'profile',
                    'href'  => route('users.show', $seller->id),
                ],
            ]

        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'name'          => 'name',
            'email'         => 'email',
            'isVerified'    => 'verified',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'identifier'    => 'id',
            'name'          => 'name',
            'email'         => 'email',
            'isVerified'    => 'verified',
            'creationDate'  => 'created_at',
            'lastChange'    => 'updated_at',
            'deletedDate'   => 'deleted_at',
        ];

        $attributes = array_flip($attributes);

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
