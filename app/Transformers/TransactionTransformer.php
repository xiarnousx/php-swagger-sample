<?php

namespace App\Transformers;

use App\Transaction;
use League\Fractal\TransformerAbstract;

class TransactionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $trans)
    {

        return [
            'identifier'    => (int) $trans->id,
            'quantity'      => (string) $trans->quantity,
            'buyer'         => (string) $trans->buyer_id,
            'product'       => (string) $trans->product_id,
            'creationDate'  => (string) $trans->created_at,
            'lastChange'    => (string) $trans->updated_at,
            'deletedDate'   => isset($trans->deleted_at) ? (string) $trans->deleted_at : null,



            'links' => [
                [
                    'rel'    => 'self',
                    'href'   => route('transactions.show', $trans->id),
                ],

                [
                    'rel'    => 'transaction.categories',
                    'href'   => route('transactions.categories.index', $trans->id),
                ],

                [
                    'rel'    => 'transaction.seller',
                    'href'   => route('transactions.sellers.index', $trans->id),
                ],

                [
                    'rel'    => 'product',
                    'href'   => route('products.show', $trans->product_id),
                ],

                [
                    'rel'    => 'buyer',
                    'href'   => route('buyers.show', $trans->buyer_id),
                ],
            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes =  [
            'identifier'       => 'id',
            'name'             => 'name',
            'quantity'         => 'quantity',
            'buyer'            => 'buyer_id',
            'product'          => 'product_id',
            'creationDate'     => 'created_at',
            'lastChange'       => 'updated_at',
            'deletedDate'      => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes =  [
            'identifier'       => 'id',
            'name'             => 'name',
            'quantity'         => 'quantity',
            'buyer'            => 'buyer_id',
            'product'          => 'product_id',
            'creationDate'     => 'created_at',
            'lastChange'       => 'updated_at',
            'deletedDate'      => 'deleted_at',
        ];

        $attributes = array_flip($attributes);

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
