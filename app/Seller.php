<?php

namespace App;


use App\Scopes\SellerScope;

class Seller extends User
{
    public $transformer = Transformers\SellerTransformer::class;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SellerScope());
    }


    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
