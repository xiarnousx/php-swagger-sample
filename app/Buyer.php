<?php

namespace App;

use App\Scopes\BuyerScope;

/**
 *
 * Class Buyer
 * @package App
 */
class Buyer extends User
{
    public $transformer = Transformers\BuyerTransformer::class;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope( new BuyerScope());
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
