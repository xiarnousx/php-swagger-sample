<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;

class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $transformer)
    {
        $transformedInput = [];

        foreach($request->request->all() as $input => $val) {
            $transformedInput[$transformer::originalAttribute($input)] = $val;
        }

        $request->replace($transformedInput);

        $response =  $next($request);

        if (isset($response->exception) && $response->exception instanceof ValidationException) {
            $data = $response->getData();

            $transformedErrors  = [];

            foreach($data->error as $field => $error) {
                $transformedFiled = $transformer::transformedAttribute($field);

                $transformedErrors[$transformedFiled] =
                    str_replace($field, $transformedFiled, $error);
            }

            $data->error = $transformedErrors;
            $response->setData($data);
        }

        return $response;
    }
}
