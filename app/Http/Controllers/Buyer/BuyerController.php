<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BuyerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @SWG\Get(
     *     path="/buyers",
     *     tags={"buyers"},
     *     summary="List All Buyers",
     *     description="",
     *     operationId="buyers.index",
     *     produces={"application/json"},
     *     @SWG\Parameter(name="sort_by", type="string", in="query", required=false, description="Sort By Field"),
     *     @SWG\Parameter(name="per_page", type="integer", in="query", required=false, description="Number of records per page"),
     *     @SWG\Parameter(name="Identifier",in="query",description="Filter buyers by id",required=false,type="integer"),
     *     @SWG\Parameter(name="name",in="query",description="Filter buyers by name",required=false,type="string"),
     *     @SWG\Parameter(name="isVerified",in="query",description="Filter buyers by isVerified",required=false,type="boolean"),
     *     @SWG\Parameter(name="creationDate",in="query",description="Filter buyers by creationDate",required=false,type="string"),
     *     @SWG\Parameter(name="lastChange",in="query",description="Filter buyers by lastChange",required=false,type="string"),
     *
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Success Response",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Buyer",
     *                  type="array"
     *              ),
     *              @SWG\Property(
     *                  property="meta",
     *                  ref="#/definitions/Pagination",
     *              ),
     *          ),
     *     ),
     *
     *     @SWG\Response(
     *          response=404,
     *          description="Error Response",
     *          @SWG\Schema(ref="#/definitions/Error")
     *     ),
     *
     * )
     *
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buyers = Buyer::all();
        return $this->showAll($buyers);
    }


    /**
     * @SWG\Get(
     *     path="/buyers/{id}",
     *     tags={"buyers"},
     *     operationId="buyers.show",
     *     summary="Fetch Buyer Details",
     *     @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Success Response",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Buyer",
     *                  type="object"
     *              ),
     *          ),
     *     ),
     *
     *     @SWG\Response(
     *          response=404,
     *          description="Error Response",
     *          @SWG\Schema(ref="#/definitions/Error")
     *     ),
     * )
     *
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buyer $buyer)
    {
        //$buyer = Buyer::has('transactions')->findOrFail($id);
        return $this->showOne($buyer);
    }

}
