<?php

namespace App\Http\Controllers\Category;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index','show']);
        $this->middleware('auth:api')->except(['index','show']);
        $this->middleware('transform.input:' . CategoryTransformer::class)
            ->only(['store', 'update']);
    }

    /**
     * @SWG\Get(
     *     path="/categories",
     *     tags={"categories"},
     *     operationId="categories.show",
     *     summary="List of Categories",
     *     @SWG\Parameter(name="sort_by", type="string", in="query", required=false, description="Sort By Field"),
     *     @SWG\Parameter(name="per_page", type="integer", in="query", required=false, description="Number of records per page"),
     *     @SWG\Parameter(name="Identifier",in="query",description="Filter categories by id",required=false,type="integer"),
     *     @SWG\Parameter(name="title",in="query",description="Filter categories by title",required=false,type="string"),
     *     @SWG\Parameter(name="creationDate",in="query",description="Filter buyers by creationDate",required=false,type="string"),
     *     @SWG\Parameter(name="lastChange",in="query",description="Filter buyers by lastChange",required=false,type="string"),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Success Response",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Category",
     *                  type="array"
     *              ),
     *              @SWG\Property(
     *                  property="meta",
     *                  ref="#/definitions/Pagination",
     *              ),
     *          ),
     *     ),
     *
     *     @SWG\Response(
     *          response=404,
     *          description="Error Response",
     *          @SWG\Schema(ref="#/definitions/Error")
     *     ),
     * )
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return $this->showAll($categories);
    }


    /**
     * @SWG\Post(
     *     path="/categories",
     *     tags={"categories"},
     *     summary="Create new Category",
     *     @SWG\Parameter(name="category", in="body", required=true, @SWG\Schema(ref="#/definitions/Category")),
     *
     *     @SWG\Response(response=200, description="success", @SWG\Schema(ref="#/definitions/Category")),
     *     @SWG\Response(response=404, description="error not found", @SWG\Schema(ref="#/definitions/Error")),
     *     @SWG\Response(response=422, description="validation error"),
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'          => 'required',
            'description'   => 'required',
        ];

        $this->validate($request, $rules);

        $category = Category::create($request->all());

        return $this->showOne($category, 201);
    }

    /**
     * @SWG\Get(
     *     path="/categories/{id}",
     *     tags={"categories"},
     *     operationId="categories.show",
     *     summary="Fetch Category Details",
     *     @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Success Response",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Category",
     *                  type="object"
     *              ),
     *          ),
     *     ),
     *
     *     @SWG\Response(
     *          response=404,
     *          description="Error Response",
     *          @SWG\Schema(ref="#/definitions/Error")
     *     ),
     * )
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return $this->showOne($category);
    }

    /**
     * @SWG\Put(
     *     path="/categories/{id}",
     *     tags={"categories"},
     *     summary="Update Category Entry",
     *
     *     @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *     @SWG\Parameter(name="category", in="body", required=false, @SWG\Schema(ref="#/definitions/Category")),
     *
     *     @SWG\Response(response=200, description="success", @SWG\Schema(ref="#/definitions/Category")),
     *     @SWG\Response(response=404, description="error not found", @SWG\Schema(ref="#/definitions/Error")),
     *     @SWG\Response(response=422, description="validation error"),
     *
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->fill($request->intersect([
            'name',
            'description',
        ]));

        if ($category->isClean()) {
            return $this->errorResponse('You need to specify any different valut to update', 422);
        }

        $category->save();

        return $this->showOne($category);
    }

    /**
     * @SWG\Delete(
     *      path="/categories/{id}",
     *      tags={"categories"},
     *      summary="Remove Category Entry",
     *      @SWG\Parameter(name="id", in="path", required=true, type="integer"),
     *      @SWG\Response(response=200, description="success", @SWG\Schema(ref="#/definitions/Category")),
     *      @SWG\Response(response=404, description="error not found", @SWG\Schema(ref="#/definitions/Error")),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return $this->showOne($category);
    }
}
