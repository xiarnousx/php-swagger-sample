<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;


/**
 * @SWG\Swagger(
 *     basePath="/",
 *     @SWG\Info(
 *          title="Buyer/Seller API",
 *          version="1.0.0"
 *     ),
 *
 *     @SWG\Definition(
 *          definition="Error",
 *          required={"error", "code"},
 *          @SWG\Property(property="error", type="string"),
 *          @SWG\Property(property="code", type="integer"),
 *     ),
 *
 *
 *    @SWG\Definition(
 *         definition="Pagination",
 *         @SWG\Property(
 *            property="pagination",
 *            type="object",
 *            @SWG\Property(property="total", type="integer"),
 *            @SWG\Property(property="count", type="integer"),
 *            @SWG\Property(property="per_page", type="integer"),
 *            @SWG\Property(property="current_page", type="integer"),
 *            @SWG\Property(property="total_pages", type="integer"),
 *            @SWG\Property(
 *                property="linkst",
 *                type="object",
 *                @SWG\Property(property="next", type="string"),
 *                @SWG\Property(property="previous", type="string"),
 *            ),
 *         ),
 *    ),
 * )
 *
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth:api');
    }
}
