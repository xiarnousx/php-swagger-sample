<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \App\User::truncate();
        \App\Category::truncate();
        \App\Product::truncate();
        \App\Transaction::truncate();
        \Illuminate\Support\Facades\DB::table('category_product')->truncate();

        \App\User::flushEventListeners();
        \App\Category::flushEventListeners();
        \App\Product::flushEventListeners();
        \App\Transaction::flushEventListeners();

        $numUsers = 2000;
        $numCategories = 30;
        $numProducts = 1000;
        $numTransactions = 1000;

        factory(\App\User::class, $numUsers)->create();
        factory(\App\Category::class, $numCategories)->create();
        factory(\App\Product::class, $numProducts)->create()->each(function($product){
            $categories = \App\Category::all()->random(mt_rand(1,5))->pluck('id');
            $product->categories()->attach($categories);
        });

        factory(\App\Transaction::class, $numTransactions)->create();
    }
}
